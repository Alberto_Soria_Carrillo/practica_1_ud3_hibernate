package bbdd;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Montura {
    private int id;
    private String marca;
    private String modelo;
    private double precio;
    private List<Factura> factura;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "marca", nullable = true, length = 45)
    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    @Basic
    @Column(name = "modelo", nullable = true, length = 45)
    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    @Basic
    @Column(name = "precio", nullable = true, precision = 0)
    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Montura montura = (Montura) o;
        return id == montura.id &&
                Double.compare(montura.precio, precio) == 0 &&
                Objects.equals(marca, montura.marca) &&
                Objects.equals(modelo, montura.modelo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, marca, modelo, precio);
    }

    @ManyToMany(mappedBy = "montura")
    public List<Factura> getFactura() {
        return factura;
    }

    public void setFactura(List<Factura> factura) {
        this.factura = factura;
    }
}
