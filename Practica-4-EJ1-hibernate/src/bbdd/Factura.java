package bbdd;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class Factura {
    private int id;
    private double precioTotal;
    private Date fecha;
    private String nameOptica;
    private String direccion;
    private String telefono;
    private Cliente cliente;
    private List<OjoIzq> ojo_izq;
    private List<OjoDrch> ojo_drch;
    private List<Montura> montura;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "precio_total", nullable = true, precision = 0)
    public double getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(double precioTotal) {
        this.precioTotal = precioTotal;
    }

    @Basic
    @Column(name = "fecha", nullable = true)
    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Basic
    @Column(name = "name_optica", nullable = true, length = 45)
    public String getNameOptica() {
        return nameOptica;
    }

    public void setNameOptica(String nameOptica) {
        this.nameOptica = nameOptica;
    }

    @Basic
    @Column(name = "direccion", nullable = true, length = 45)
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Basic
    @Column(name = "telefono", nullable = true, length = 45)
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Factura factura = (Factura) o;
        return id == factura.id &&
                Double.compare(factura.precioTotal, precioTotal) == 0 &&
                Objects.equals(fecha, factura.fecha) &&
                Objects.equals(nameOptica, factura.nameOptica) &&
                Objects.equals(direccion, factura.direccion) &&
                Objects.equals(telefono, factura.telefono);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, precioTotal, fecha, nameOptica, direccion, telefono);
    }

    @ManyToOne
    @JoinColumn(name = "id_cliente", referencedColumnName = "id")
    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @ManyToMany
    @JoinTable(name = "factura_ojo_izq", catalog = "", schema = "optica2", joinColumns = @JoinColumn(name = "id_factura", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "id_ojo_izq", referencedColumnName = "id"))
    public List<OjoIzq> getOjo_izq() {
        return ojo_izq;
    }

    public void setOjo_izq(List<OjoIzq> ojo_izq) {
        this.ojo_izq = ojo_izq;
    }

    @ManyToMany
    @JoinTable(name = "factura_ojo_drch", catalog = "", schema = "optica2", joinColumns = @JoinColumn(name = "id_factura", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "id_ojo_drch", referencedColumnName = "id"))
    public List<OjoDrch> getOjo_drch() {
        return ojo_drch;
    }

    public void setOjo_drch(List<OjoDrch> ojo_drch) {
        this.ojo_drch = ojo_drch;
    }

    @ManyToMany
    @JoinTable(name = "factura_montura", catalog = "", schema = "optica2", joinColumns = @JoinColumn(name = "id_factura", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "id_montura", referencedColumnName = "id"))
    public List<Montura> getMontura() {
        return montura;
    }

    public void setMontura(List<Montura> montura) {
        this.montura = montura;
    }
}
