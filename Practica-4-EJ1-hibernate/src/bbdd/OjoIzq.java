package bbdd;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "ojo_izq", schema = "optica2", catalog = "")
public class OjoIzq {
    private int id;
    private boolean hipermetropia;
    private boolean astigmatismo;
    private boolean reducido;
    private double graduacion;
    private String tipo;
    private List<Factura> factura;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "hipermetropia", nullable = true)
    public boolean isHipermetropia() {
        return hipermetropia;
    }

    public void setHipermetropia(boolean hipermetropia) {
        this.hipermetropia = hipermetropia;
    }

    @Basic
    @Column(name = "astigmatismo", nullable = true)
    public boolean isAstigmatismo() {
        return astigmatismo;
    }

    public void setAstigmatismo(boolean astigmatismo) {
        this.astigmatismo = astigmatismo;
    }

    @Basic
    @Column(name = "reducido", nullable = true)
    public boolean isReducido() {
        return reducido;
    }

    public void setReducido(boolean reducido) {
        this.reducido = reducido;
    }

    @Basic
    @Column(name = "graduacion", nullable = true, precision = 0)
    public double getGraduacion() {
        return graduacion;
    }

    public void setGraduacion(double graduacion) {
        this.graduacion = graduacion;
    }

    @Basic
    @Column(name = "tipo", nullable = true, length = 6)
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OjoIzq ojoIzq = (OjoIzq) o;
        return id == ojoIzq.id &&
                hipermetropia == ojoIzq.hipermetropia &&
                astigmatismo == ojoIzq.astigmatismo &&
                reducido == ojoIzq.reducido &&
                Double.compare(ojoIzq.graduacion, graduacion) == 0 &&
                Objects.equals(tipo, ojoIzq.tipo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, hipermetropia, astigmatismo, reducido, graduacion, tipo);
    }

    @ManyToMany(mappedBy = "ojo_izq")
    public List<Factura> getFactura() {
        return factura;
    }

    public void setFactura(List<Factura> factura) {
        this.factura = factura;
    }
}
