package constantes;

/**
 * Clase Const, guarda las constantes del programa
 *
 * @author Alberto Soria Carrillo
 */
public class Const {

    public static final String TABLE_OJO_IZQ = "OjoIzq";
    public static final String TABLE_OJO_DRCH = "OjoDrch";
    public static final String TABLE_MONTURA = "Montura";
    public static final String TABLE_CLIENTE = "Cliente";
    public static final String TABLE_FACTURA = "Factura";
    public static final String TIPO_SOL = "Sol";
    public static final String TIPO_NORMAL = "Normal";
    public static final String BTN_CONECTAR = "Conectar";
    public static final String BTN_DESCONECTAR = "Desconectar";
    public static final String BTN_VISTA_OJO = "btn_vista_ojo";
    public static final String BTN_CANCELAR = "cancelar";
    public static final String BTN_ANIDAR_CLIENTE = "anidar_cliente_button";
    public static final String BTN_ANIDAR_CRISTALES_IZQ = "anida_cristales_izquierdo_button";
    public static final String BTN_ANIDAR_CRISTALES_DRCH = "anida_cristales_derecho_button";
    public static final String BTN_SELECCIONAR_CLIENTE = "seleccionar_cliente_button";
    public static final String BTN_ANIDAR_MONTURA = "anidar_montura_button";
    public static final String BTN_CREAR_FACTURA = "crear_factura_button";
    public static final String BTN_ELIMINAR_SELECCION = "eliminar_seleccion_button";
    public static final String BTN_CREAR_CLIENTE = "crear_cliente_button";
    public static final String BTN_CREAR_CRISTAL_IZQUIERDO = "crear_cristal_izquierdo_button";
    public static final String BTN_CREAR_CRISTAL_DERECHO = "crear_cristal_derecho_button";
    public static final String BTN_CREAR_MONTURA = "crear_montura_button";
    public static final String BTN_MODIFICAR_CLIENTE = "modificar_cliente_button";
    public static final String BTN_MODIFICAR_CRISTAL_IZQUIERDO = "modificar_cristal_izquierdo_button";
    public static final String BTN_MODIFICAR_CRISTAL_DERECHO = "modificar_cristal_derecho_button";
    public static final String BTN_MODIFICAR_MONTURA = "modificar_montura_button";
    public static final String BTN_MODIFICAR = "modificar_button";
    public static final String BTN_BUSCAR = "buscar_button";
}
