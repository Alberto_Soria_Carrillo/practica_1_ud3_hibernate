package conexion;

import bbdd.*;
import constantes.Const;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import javax.persistence.Query;
import javax.swing.*;
import java.util.ArrayList;

/**
 * Clase Conexion, se encarga de las conexion y transferencias de informacion con la BBDD
 *
 * @author Alberto Soria Carrillo
 */
public class Conexion {
    // Atributos
    public SessionFactory sessionFactory;

    /**
     * Metodo de cierre de sesion
     */
    public void desconectar() {
        //cierro la factoria de sesiones
        if (sessionFactory != null && sessionFactory.isOpen())
            sessionFactory.close();
    }

    /**
     * Metodo de conexion mediante la declaracion de hibernate
     */
    public void conectar() {
        // Crea un objeto sobre el que se cargara la configuracion guardada en un XML
        Configuration configuracion = new Configuration();
        // Cargo el fichero hibernate.cfg.xml
        configuracion.configure("hibernate.cfg.xml");

        //indico las clases mapeadas
        configuracion.addAnnotatedClass(Cliente.class);
        configuracion.addAnnotatedClass(Factura.class);
        configuracion.addAnnotatedClass(FacturaMontura.class);
        configuracion.addAnnotatedClass(FacturaOjoDrch.class);
        configuracion.addAnnotatedClass(FacturaOjoIzq.class);
        configuracion.addAnnotatedClass(Montura.class);
        configuracion.addAnnotatedClass(OjoDrch.class);
        configuracion.addAnnotatedClass(OjoIzq.class);

        //creamos un objeto ServiceRegistry para los parametros de configuracion
        //esta clase se encarga de gestionar y proveer servicios
        StandardServiceRegistry standardServiceRegistry = new StandardServiceRegistryBuilder().applySettings(
                configuracion.getProperties()).build();

        //finalmente creamos un objeto sessionFactory a partir de la configuracion y los registros

        sessionFactory = configuracion.buildSessionFactory(standardServiceRegistry);
    }


    /**
     * Carga la información de la BBDD en el JComboBox
     *
     * @param comboBox
     * @param table
     */
    public void cargarComboBox(JComboBox comboBox, String table) {
        // crea la sesion de la conexión
        Session sesion = sessionFactory.openSession();
        // crea la peticion sql
        Query query = sesion.createQuery("FROM " + table);

        comboBox.removeAllItems();
        String aux = "";

        switch (table) {
            case Const.TABLE_CLIENTE:
                ArrayList<Cliente> listaClientes = (ArrayList<Cliente>) query.getResultList();
                for (Cliente cliente : listaClientes) {
                    aux = cliente.getId() + " - " + cliente.getNombre() + " " + cliente.getApellido1() + " " + cliente.getApellido2();
                    comboBox.addItem(aux);
                }
                sesion.close();
                break;
            case Const.TABLE_MONTURA:
                ArrayList<Montura> listaMontura = (ArrayList<Montura>) query.getResultList();
                for (Montura montura : listaMontura) {
                    aux = montura.getId() + " - " + montura.getMarca() + " " + montura.getModelo() + " " + montura.getPrecio();
                    comboBox.addItem(aux);
                }
                sesion.close();
                break;
            case Const.TABLE_OJO_DRCH:
                ArrayList<OjoDrch> listaOjoDerecho = (ArrayList<OjoDrch>) query.getResultList();
                for (OjoDrch ojo : listaOjoDerecho) {
                    aux = ojo.getId() + " - " + ojo.getGraduacion() + " " + ojo.getTipo() + " " + ojo.isHipermetropia() + " " + ojo.isAstigmatismo() + " " + ojo.isReducido();
                    comboBox.addItem(aux);
                }
                sesion.close();
                break;
            case Const.TABLE_OJO_IZQ:
                ArrayList<OjoIzq> listaOjoIzquierdo = (ArrayList<OjoIzq>) query.getResultList();
                for (OjoIzq ojo : listaOjoIzquierdo) {
                    aux = ojo.getId() + " - " + ojo.getGraduacion() + " " + ojo.getTipo() + " " + ojo.isHipermetropia() + " " + ojo.isAstigmatismo() + " " + ojo.isReducido();
                    comboBox.addItem(aux);
                }
                sesion.close();
                break;
            case Const.TABLE_FACTURA:
                ArrayList<Factura> listaFactura = (ArrayList<Factura>) query.getResultList();
                for (Factura factura : listaFactura) {
                    aux = factura.getId() + " - " + factura.getFecha() + " " + factura.getPrecioTotal() + " " + factura.getCliente();
                    comboBox.addItem(aux);
                }
                sesion.close();
                break;
        }
    }

    /**
     * Carga la informacion de la BBDD en el DefaultListModel
     *
     * @param list
     * @param table
     */
    public void cargarList(DefaultListModel list, String table) {

        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM " + table);
        String aux = "";
        list.removeAllElements();
        switch (table) {
            case Const.TABLE_CLIENTE:
                ArrayList<Cliente> listaClientes = (ArrayList<Cliente>) query.getResultList();
                sesion.close();
                for (Cliente cliente : listaClientes) {
                    aux = cliente.getId() + " - " + cliente.getNombre() + " " + cliente.getApellido1() + " " + cliente.getApellido2();
                    list.addElement(aux);
                }
                break;
            case Const.TABLE_MONTURA:
                ArrayList<Montura> listaMontura = (ArrayList<Montura>) query.getResultList();
                sesion.close();
                for (Montura montura : listaMontura) {
                    aux = montura.getId() + " - " + montura.getMarca() + " " + montura.getModelo() + " " + montura.getPrecio();
                    list.addElement(aux);
                }
                break;
            case Const.TABLE_OJO_DRCH:
                ArrayList<OjoDrch> listaOjoDerecho = (ArrayList<OjoDrch>) query.getResultList();
                sesion.close();
                for (OjoDrch ojo : listaOjoDerecho) {
                    aux = ojo.getId() + " - " + ojo.getGraduacion() + " " + ojo.getTipo() + " " + ojo.isHipermetropia() + " " + ojo.isAstigmatismo() + " " + ojo.isReducido();
                    list.addElement(aux);
                }
                break;
            case Const.TABLE_OJO_IZQ:
                ArrayList<OjoIzq> listaOjoIzquierdo = (ArrayList<OjoIzq>) query.getResultList();
                sesion.close();
                for (OjoIzq ojo : listaOjoIzquierdo) {
                    aux = ojo.getId() + " - " + ojo.getGraduacion() + " " + ojo.getTipo() + " " + ojo.isHipermetropia() + " " + ojo.isAstigmatismo() + " " + ojo.isReducido();
                    list.addElement(aux);
                }
                break;
            case Const.TABLE_FACTURA:
                ArrayList<Factura> listaFactura = (ArrayList<Factura>) query.getResultList();
                sesion.close();

                for (int i = 0; i < listaFactura.size(); i++) {

                    aux = listaFactura.get(i).getId() + " - " + listaFactura.get(i).getFecha() + " - "
                            + listaFactura.get(i).getPrecioTotal() + "€ - "
                            + listaFactura.get(i).getCliente().getId()
                            + " " + listaFactura.get(i).getCliente().getNombre() + " "
                            + listaFactura.get(i).getCliente().getApellido1();

                    list.addElement(aux);
                }
                break;
        }
    }

    /**
     * Inserta un OjoIzq en la BBDD
     *
     * @param ojoIzq
     */
    public void insertInto(OjoIzq ojoIzq) {
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(ojoIzq);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     * Inserta OjoDrch en la BBDD
     *
     * @param ojoDrch
     */
    public void insertInto(OjoDrch ojoDrch) {
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(ojoDrch);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     * Inserta Cliente en la BBDD
     *
     * @param cliente
     */
    public void insertInto(Cliente cliente) {
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(cliente);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     * Inserta una factura en la BBDD
     *
     * @param factura
     */
    public void insertInto(Factura factura) {

        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(factura);
        sesion.getTransaction().commit();

        sesion.close();

    }

    /**
     * Inserta Montura en la BBDD
     *
     * @param montura
     */
    public void insertInto(Montura montura) {
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(montura);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     * Busca un cliente por su id en la BBDD y debuelve un Objeto de la clase Cliente con dicha información
     *
     * @param s
     * @return
     */
    public Cliente buscarCliente(String s) {

        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM " + Const.TABLE_CLIENTE + " WHERE id = " + s);
        ArrayList<Cliente> listaClientes = (ArrayList<Cliente>) query.getResultList();
        sesion.close();

        Cliente cliente = new Cliente();

        for (Cliente aux : listaClientes) {
            cliente.setId(aux.getId());
            cliente.setNombre(aux.getNombre());
            cliente.setApellido1(aux.getApellido1());
            cliente.setApellido2(aux.getApellido2());
            cliente.setFechaNacimiento(aux.getFechaNacimiento());
            cliente.setGenero(aux.isGenero());
            cliente.setDireccion(aux.getDireccion());
            cliente.setFactura(aux.getFactura());
        }

        return cliente;
    }

    /**
     * Fracciona el String y debuelve el primer fragmento
     *
     * @param string
     * @return
     */
    public String sacarId(String string) {
        String[] aux = string.split(" - ");
        String id = aux[0];
        return id;

    }

    /**
     * Busca las monturas por su id en la BBDD y debuelve un Objeto de la clase ArrayList con dicha información
     *
     * @param dlm
     * @return
     */
    public ArrayList<Montura> buscarMontura(DefaultListModel dlm) {

        ArrayList<Montura> monturas = new ArrayList<>();
        Session sesion = sessionFactory.openSession();

        for (int i = 0; i < dlm.getSize(); i++) {
            Query query = sesion.createQuery("FROM " + Const.TABLE_MONTURA + " WHERE id = " + sacarId(dlm.get(i).toString()));
            ArrayList<Montura> aux = (ArrayList<Montura>) query.getResultList();

            for (int o = 0; o < aux.size(); o++) {
                monturas.add(aux.get(o));
            }
        }
        sesion.close();
        return monturas;
    }

    /**
     * Busca los Ojos derechos por su id en la BBDD y debuelve un Objeto de la clase ArrayList con dicha información
     *
     * @param dlm
     * @return
     */
    public ArrayList<OjoDrch> buscarOjoDrch(DefaultListModel dlm) {

        ArrayList<OjoDrch> ojoDrches = new ArrayList<>();
        Session sesion = sessionFactory.openSession();

        for (int i = 0; i < dlm.getSize(); i++) {
            Query query = sesion.createQuery("FROM " + Const.TABLE_OJO_DRCH + " WHERE id = " + sacarId(dlm.get(i).toString()));
            ArrayList<OjoDrch> aux = (ArrayList<OjoDrch>) query.getResultList();

            for (int o = 0; o < aux.size(); o++) {
                ojoDrches.add(aux.get(o));
            }
        }
        sesion.close();
        return ojoDrches;

    }

    /**
     * Busca los Ojos izquierdos por su id en la BBDD y debuelve un Objeto de la clase ArrayList con dicha información
     *
     * @param dlm
     * @return
     */
    public ArrayList<OjoIzq> buscarOjoIzq(DefaultListModel dlm) {

        ArrayList<OjoIzq> ojoIzqs = new ArrayList<>();
        Session sesion = sessionFactory.openSession();

        for (int i = 0; i < dlm.getSize(); i++) {
            Query query = sesion.createQuery("FROM " + Const.TABLE_OJO_IZQ + " WHERE id = " + sacarId(dlm.get(i).toString()));
            ArrayList<OjoIzq> aux = (ArrayList<OjoIzq>) query.getResultList();

            for (int o = 0; o < aux.size(); o++) {
                ojoIzqs.add(aux.get(o));
            }
        }
        sesion.close();
        return ojoIzqs;

    }

    /**
     * Elimina de la BBDD el elemento seleccionado de la lista
     *
     * @param toString
     * @param table
     */
    public void eliminar(String toString, String table) {
        Session sesion = sessionFactory.openSession();
        Query query;
        String aux = "";
        int id = Integer.valueOf(sacarId(toString));

        switch (table) {
            case Const.TABLE_CLIENTE:
                query = sesion.createQuery("FROM " + Const.TABLE_CLIENTE + " WHERE id = " + id);
                ArrayList<Cliente> listaClientes = (ArrayList<Cliente>) query.getResultList();
                for (int i = 0; i < listaClientes.size(); i++) {
                    if (listaClientes.get(i).getId() == id) {

                        Cliente cliente = listaClientes.get(i);
                        sesion.beginTransaction();
                        sesion.delete(cliente);
                        sesion.getTransaction().commit();
                        break;
                    }
                }
                sesion.close();
                break;
            case Const.TABLE_MONTURA:
                query = sesion.createQuery("FROM " + Const.TABLE_MONTURA + " WHERE id = " + id);
                ArrayList<Montura> listaMontura = (ArrayList<Montura>) query.getResultList();
                for (int i = 0; i < listaMontura.size(); i++) {
                    if (listaMontura.get(i).getId() == id) {

                        Montura montura = listaMontura.get(i);
                        sesion.beginTransaction();
                        sesion.delete(montura);
                        sesion.getTransaction().commit();
                        break;
                    }
                }
                sesion.close();
                break;
            case Const.TABLE_OJO_DRCH:
                query = sesion.createQuery("FROM " + Const.TABLE_OJO_DRCH + " WHERE id = " + id);
                ArrayList<OjoDrch> listaOjoDerecho = (ArrayList<OjoDrch>) query.getResultList();
                for (int i = 0; i < listaOjoDerecho.size(); i++) {
                    if (listaOjoDerecho.get(i).getId() == id) {

                        OjoDrch ojo = listaOjoDerecho.get(i);
                        sesion.beginTransaction();
                        sesion.delete(ojo);
                        sesion.getTransaction().commit();
                        break;
                    }
                }
                sesion.close();
                break;
            case Const.TABLE_OJO_IZQ:
                query = sesion.createQuery("FROM " + Const.TABLE_OJO_IZQ + " WHERE id = " + id);

                ArrayList<OjoIzq> listaOjoIzquierdo = (ArrayList<OjoIzq>) query.getResultList();
                for (int i = 0; i < listaOjoIzquierdo.size(); i++) {
                    if (listaOjoIzquierdo.get(i).getId() == id) {

                        OjoIzq ojo = listaOjoIzquierdo.get(i);
                        sesion.beginTransaction();
                        sesion.delete(ojo);
                        sesion.getTransaction().commit();
                        break;
                    }
                }
                sesion.close();
                break;
            case Const.TABLE_FACTURA:
                query = sesion.createQuery("FROM " + Const.TABLE_FACTURA + " WHERE id = " + id);

                ArrayList<Factura> listaFactura = (ArrayList<Factura>) query.getResultList();

                for (int i = 0; i < listaFactura.size(); i++) {
                    if (listaFactura.get(i).getId() == id) {
                        sesion.beginTransaction();
                        sesion.delete(listaFactura);
                        sesion.getTransaction().commit();
                        break;
                    }
                }
                sesion.close();
                break;
        }

    }

    /**
     * busca la inforcaión en la tabla segun el id y la debuelve formateada
     *
     * @param table
     * @param buscar
     * @return
     */
    public String buscar(String table, String buscar) {
        String aux = "";
        Session sesion = sessionFactory.openSession();
        Query query;
        switch (table) {
            case Const.TABLE_CLIENTE:
                query = sesion.createQuery("FROM " + Const.TABLE_CLIENTE + " WHERE id =" + buscar);
                ArrayList<Cliente> listaClientes = (ArrayList<Cliente>) query.getResultList();
                sesion.close();
                for (Cliente cliente : listaClientes) {
                    aux = cliente.getId() + " - " + cliente.getNombre() + " " + cliente.getApellido1() + " " + cliente.getApellido2();
                }
                break;
            case Const.TABLE_MONTURA:
                query = sesion.createQuery("FROM " + Const.TABLE_MONTURA + " WHERE id =" + buscar);
                ArrayList<Montura> listaMontura = (ArrayList<Montura>) query.getResultList();
                sesion.close();
                for (Montura montura : listaMontura) {
                    aux = montura.getId() + " - " + montura.getMarca() + " " + montura.getModelo() + " " + montura.getPrecio();
                }
                break;
            case Const.TABLE_OJO_DRCH:
                query = sesion.createQuery("FROM " + Const.TABLE_OJO_DRCH + " WHERE id =" + buscar);
                ArrayList<OjoDrch> listaOjoDerecho = (ArrayList<OjoDrch>) query.getResultList();
                sesion.close();
                for (OjoDrch ojo : listaOjoDerecho) {
                    aux = ojo.getId() + " - " + ojo.getGraduacion() + " " + ojo.getTipo() + " " + ojo.isHipermetropia() + " " + ojo.isAstigmatismo() + " " + ojo.isReducido();
                }
                break;
            case Const.TABLE_OJO_IZQ:
                query = sesion.createQuery("FROM " + Const.TABLE_OJO_IZQ + " WHERE id =" + buscar);
                ArrayList<OjoIzq> listaOjoIzquierdo = (ArrayList<OjoIzq>) query.getResultList();
                sesion.close();
                for (OjoIzq ojo : listaOjoIzquierdo) {
                    aux = ojo.getId() + " - " + ojo.getGraduacion() + " " + ojo.getTipo() + " " + ojo.isHipermetropia() + " " + ojo.isAstigmatismo() + " " + ojo.isReducido();
                }
                break;
            case Const.TABLE_FACTURA:
                query = sesion.createQuery("FROM " + Const.TABLE_FACTURA + " WHERE id =" + buscar);
                ArrayList<Factura> listaFactura = (ArrayList<Factura>) query.getResultList();
                sesion.close();
                for (Factura factura : listaFactura) {
                    aux = factura.getId() + " - " + factura.getFecha() + " " + factura.getPrecioTotal() + "€ - " + factura.getCliente().getId() + " " + factura.getCliente().getNombre() + " " + factura.getCliente().getApellido1();
                }
                break;
        }


        return aux;
    }

    /**
     * busca el id en la tabla ojo_drch
     *
     * @param info
     * @return
     */
    public OjoDrch buscarOjoDrch(String info) {

        OjoDrch ojoDrch = new OjoDrch();

        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM " + Const.TABLE_OJO_DRCH + " WHERE id = " + info);
        ArrayList<OjoDrch> aux = (ArrayList<OjoDrch>) query.getResultList();

        for (int o = 0; o < aux.size(); o++) {
            ojoDrch = (aux.get(o));
        }

        sesion.close();
        return ojoDrch;
    }

    /**
     * busca el id en la tabla ojo_izq
     *
     * @param info
     * @return
     */
    public OjoIzq buscarOjoIzq(String info) {
        OjoIzq ojo = new OjoIzq();

        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM " + Const.TABLE_OJO_IZQ + " WHERE id = " + info);
        ArrayList<OjoIzq> aux = (ArrayList<OjoIzq>) query.getResultList();

        for (int o = 0; o < aux.size(); o++) {
            ojo = (aux.get(o));
        }

        sesion.close();
        return ojo;
    }

    /**
     * Busca el id en la tabla montura de la BBDD
     *
     * @param info
     * @return
     */
    public Montura buscarMontura(String info) {
        Montura montura = new Montura();

        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM " + Const.TABLE_MONTURA + " WHERE id = " + info);
        ArrayList<Montura> aux = (ArrayList<Montura>) query.getResultList();

        for (int o = 0; o < aux.size(); o++) {
            montura = (aux.get(o));
        }

        sesion.close();
        return montura;
    }

    /**
     * Actualiza el ojo izquierdo en la BBDD
     *
     * @param ojoIzq
     */
    public void update(OjoIzq ojoIzq) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(ojoIzq);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Actualiza el ojo derecho en la BBDD
     *
     * @param ojoDrch
     */
    public void update(OjoDrch ojoDrch) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(ojoDrch);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Actualiza la montura en la BBDD
     *
     * @param montura
     */
    public void update(Montura montura) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(montura);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Actualiza el cliente en la BBDD
     *
     * @param cliente
     */
    public void update(Cliente cliente) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(cliente);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Actualiza la factura en la BBDD
     *
     * @param factura
     */
    public void update(Factura factura) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(factura);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Busca el id maximo de la tabla facturas
     *
     * @return
     */
    public int maxIdFactura() {

        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM " + Const.TABLE_FACTURA);
        ArrayList<Factura> aux = (ArrayList<Factura>) query.getResultList();
        sesion.close();
        int maxId = 0;
        for (int o = 0; o < aux.size(); o++) {
            if (aux.get(o).getId() > maxId) {
                maxId = aux.get(o).getId();
            }
        }
        return maxId;
    }
}