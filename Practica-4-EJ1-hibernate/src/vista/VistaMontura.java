package vista;

import bbdd.Montura;
import conexion.Conexion;
import constantes.Const;
import main.Modelo;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Clase VistaMonturas, se encarga de la inserción y modificación de las monturas en la BBDD
 *
 * @author Alberto Soria Carrillo
 */
public class VistaMontura extends JFrame implements ActionListener {

    private JPanel panel1;
    private JTextField textField1;
    private JTextField textField2;
    private JTextField textField3;
    private JButton anadirMonturaButton;
    private JButton cancelarButton;
    private String table;
    private Conexion conexion;
    private JComboBox combo;
    private Float precio;
    private Modelo modelo;
    private String info;
    private Boolean aBoolean;
    private DefaultListModel defaultListModel;

    /**
     * Constructor de la clase VistaMonturas
     *
     * @param tableMontura
     * @param conexion
     * @param monturaFacturaComboBox
     */
    public VistaMontura(String tableMontura, Conexion conexion, JComboBox monturaFacturaComboBox) {
        this.table = tableMontura;
        this.conexion = conexion;
        this.combo = monturaFacturaComboBox;
        this.aBoolean = false;

        this.add(panel1);

        addActionComand();
        setActionListener(this);

        this.setUndecorated(true);
        this.setDefaultCloseOperation(3);
        this.pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true);

    }

    /**
     * Constructor de la clase VistaMontura
     *
     * @param info
     * @param table
     * @param conexion
     * @param modelo
     */
    public VistaMontura(DefaultListModel dlmModificar, String info, String table, Conexion conexion, Modelo modelo, JComboBox monturaFacturaComboBox) {

        this.table = table;
        this.modelo = modelo;
        this.conexion = conexion;
        this.info = modelo.sacarId(info);
        this.combo = monturaFacturaComboBox;
        this.aBoolean = true;
        this.anadirMonturaButton.setText("Moificar montura");
        this.defaultListModel = dlmModificar;

        Montura montura = conexion.buscarMontura(this.info);

        agregarInformacion(montura);

        this.add(panel1);

        addActionComand();
        setActionListener(this);

        this.setUndecorated(true);
        this.setDefaultCloseOperation(3);
        this.pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true);

    }

    /**
     * Agrega la información a los campos
     *
     * @param montura
     */
    private void agregarInformacion(Montura montura) {

        textField1.setText(montura.getMarca());
        textField2.setText(montura.getModelo());
        textField3.setText(String.valueOf(montura.getPrecio()));

    }

    /**
     * Agrega los ActionListener
     *
     * @param listener
     */
    private void setActionListener(ActionListener listener) {

        anadirMonturaButton.addActionListener(listener);
        cancelarButton.addActionListener(listener);

    }

    /**
     * Agrega los ActionCommand
     */
    private void addActionComand() {
        anadirMonturaButton.setActionCommand(Const.BTN_ANIDAR_MONTURA);
        cancelarButton.setActionCommand(Const.BTN_CANCELAR);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case Const.BTN_ANIDAR_MONTURA:

                if (controlNum()) {

                    if (!textField1.getText().equalsIgnoreCase("") && !textField2.getText().equalsIgnoreCase("")
                            && !textField3.getText().equalsIgnoreCase("")) {
                        if (!aBoolean) {
                            Montura montura = new Montura();
                            montura.setMarca(textField1.getText());
                            montura.setModelo(textField2.getText());
                            montura.setPrecio(precio);

                            conexion.insertInto(montura);

                        } else {

                            Montura montura = new Montura();
                            montura.setId(Integer.valueOf(info));
                            montura.setMarca(textField1.getText());
                            montura.setModelo(textField2.getText());
                            montura.setPrecio(precio);

                            conexion.update(montura);
                            conexion.cargarList(this.defaultListModel, table);
                        }
                        conexion.cargarComboBox(combo, table);
                    } else {
                        JOptionPane.showMessageDialog(null, "Los campos deben estar completos");
                    }

                    this.dispose();
                } else {
                    JOptionPane.showMessageDialog(null, "El dato numerico no es valido");
                }

                break;
            case Const.BTN_CANCELAR:
                this.dispose();
                break;
        }
    }

    /**
     * comprueba que el dato introducido se Float
     *
     * @return
     */
    private boolean controlNum() {

        try {
            precio = Float.valueOf(textField3.getText());
            return true;
        } catch (ArithmeticException | NumberFormatException ex) {
            return false;
        }

    }
}
