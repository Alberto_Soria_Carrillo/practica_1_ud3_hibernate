package vista;

import constantes.Const;

import javax.swing.*;

/**
 * Clase VistaFactur, ventana pricipal del programa
 *
 * @author Alberto Soria Carrillo
 */
public class VistaFactura extends JFrame {

    public JPanel panel1;
    public JComboBox ojoIzqFacturaComboBox;
    public JButton anadirCristalesIzquierdosButton;
    public JList ojoIzqFacturaList;
    public JComboBox ojoDrchFacturaComboBox;
    public JButton anadirCristalesDerechosButton;
    public JList ojoDrchFacturaList;
    public JComboBox monturaComboBox;
    public JButton anadirMonturasButton;
    public JList monturaFacturaList;
    public JButton seleccionarClienteButton;
    public JTextField nombreClienteFacturaTextField;
    public JComboBox clienteFacturaComboBox;
    public JButton crearFacturaButton;
    public JButton eliminarSeleccionButton;
    public JButton crearClienteButton;
    public JButton crearCristalIzquierdoButton;
    public JButton crearCristalDerechoButton;
    public JButton crearMonturaButton;
    public JButton modificarClienteButton;
    public JButton modificarCristalIzquierdoButton;
    public JButton modificarCristalDerechoButton;
    public JButton modificarMonturaButton;
    public JList facturaList;
    public JMenuBar jMenuBar;
    public JMenu menu;
    public JMenuItem item;
    public DefaultListModel dlmOjo_izq;
    public DefaultListModel dlmOjo_drch;
    public DefaultListModel dlmMontura;
    public DefaultListModel dlmFactura;

    /**
     * Constructor de la clase VistaFactura
     */
    public VistaFactura() {

        this.add(panel1);

        startElement();
        menu();
        addDefaultListModel();

        this.pack();
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(3);

        this.setVisible(true);
    }

    /**
     * Inicializa los elementos
     */
    private void startElement() {
        jMenuBar = new JMenuBar();
        menu = new JMenu("Opciones");
        item = new JMenuItem(Const.BTN_CONECTAR);

        dlmFactura = new DefaultListModel();
        dlmMontura = new DefaultListModel();
        dlmOjo_drch = new DefaultListModel();
        dlmOjo_izq = new DefaultListModel();

    }

    /**
     * Agrega los DefaultListModel
     */
    private void addDefaultListModel() {

        ojoIzqFacturaList.setModel(dlmOjo_izq);
        ojoDrchFacturaList.setModel(dlmOjo_drch);
        monturaFacturaList.setModel(dlmMontura);
        facturaList.setModel(dlmFactura);

    }

    /**
     * Agrega el menu
     */
    private void menu() {

        menu.add(item);
        jMenuBar.add(menu);
        this.setJMenuBar(jMenuBar);

    }


}
