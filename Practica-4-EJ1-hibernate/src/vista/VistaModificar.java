package vista;

import conexion.Conexion;
import constantes.Const;
import main.Modelo;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Clase VistaModifiar, permite la edicion de la informaión en la BBDD
 *
 * @author Alberto Soria Carrillo
 */
public class VistaModificar extends JFrame implements ActionListener, ListSelectionListener {

    private JPanel panel1;
    private JButton modificarButton;
    private JButton cancelarButton;
    private JList list1;
    private JButton buscarButton;
    private JTextField buscarTextField;
    private JButton eliminarButton;
    private Conexion conexion;
    private Modelo modelo;
    private VistaFactura vistaFactura;
    private String table;
    private DefaultListModel dlmModificar;

    /**
     * Contructor de la clase VistaModificar
     *
     * @param conexion
     * @param table
     * @param modelo
     * @param vistaFactura
     */
    public VistaModificar(Conexion conexion, String table, Modelo modelo, VistaFactura vistaFactura) {

        this.conexion = conexion;
        this.table = table;
        this.modelo = modelo;
        this.vistaFactura = vistaFactura;

        this.add(panel1);

        startElements();
        addDefaulListModel();
        setListListener(this);
        setActionListener(this);
        addActionCommad();

        conexion.cargarList(dlmModificar, table);

        this.setUndecorated(true);
        this.setDefaultCloseOperation(3);
        this.pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    /**
     * Agrega los ListSelectionListener
     *
     * @param listListener
     */
    private void setListListener(ListSelectionListener listListener) {
        list1.addListSelectionListener(listListener);
    }

    /**
     * Agrega el DefaultListModel al JList
     */
    private void addDefaulListModel() {
        list1.setModel(dlmModificar);
    }

    /**
     * Agrega los ActionCommand
     */
    private void addActionCommad() {
        modificarButton.setActionCommand(Const.BTN_MODIFICAR);
        cancelarButton.setActionCommand(Const.BTN_CANCELAR);
        eliminarButton.setActionCommand(Const.BTN_ELIMINAR_SELECCION);
        buscarButton.setActionCommand(Const.BTN_BUSCAR);
    }

    /**
     * Agrega los ActionListener
     *
     * @param listener
     */
    private void setActionListener(ActionListener listener) {
        modificarButton.addActionListener(listener);
        cancelarButton.addActionListener(listener);
        eliminarButton.addActionListener(listener);
        buscarButton.addActionListener(listener);
    }

    /**
     * Inicializa los elementos de la clase
     */
    private void startElements() {
        dlmModificar = new DefaultListModel();
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        switch (e.getActionCommand()) {
            case Const.BTN_MODIFICAR:
                switch (table) {
                    case Const.TABLE_OJO_DRCH:
                        VistaOjo vistaOjoDrch = new VistaOjo(dlmModificar,dlmModificar.get(list1.getSelectedIndex()).toString(),table,conexion, modelo, vistaFactura.ojoDrchFacturaComboBox);
                        break;
                    case Const.TABLE_OJO_IZQ:
                        VistaOjo vistaOjoIzq = new VistaOjo(dlmModificar,dlmModificar.get(list1.getSelectedIndex()).toString(),table,conexion, modelo, vistaFactura.ojoIzqFacturaComboBox);
                        break;
                    case Const.TABLE_MONTURA:
                        VistaMontura vistaMontura = new VistaMontura(dlmModificar,dlmModificar.get(list1.getSelectedIndex()).toString(),table,conexion, modelo,vistaFactura.monturaComboBox);
                        break;
                    case Const.TABLE_CLIENTE:
                        VistaCliente vistaCliente = new VistaCliente(dlmModificar,dlmModificar.get(list1.getSelectedIndex()).toString(),table,conexion, modelo,vistaFactura.clienteFacturaComboBox);
                        break;
                }

                break;
            case Const.BTN_CANCELAR:
                this.dispose();
                break;
            case Const.BTN_ELIMINAR_SELECCION:
                conexion.eliminar(dlmModificar.get(list1.getSelectedIndex()).toString(), table);
                dlmModificar.remove(list1.getSelectedIndex());
                modelo.agreagarInformacion(vistaFactura,conexion);
                break;
            case Const.BTN_BUSCAR:
                if (modelo.comprobarInt(buscarTextField.getText())) {
                    dlmModificar.clear();
                    dlmModificar.addElement(conexion.buscar(table, buscarTextField.getText()));
                } else if(buscarTextField.getText().equalsIgnoreCase("")){
                    conexion.cargarList(dlmModificar, table);
                }else{JOptionPane.showMessageDialog(null, "El campo busqueda debe ser un numero entero");}

                break;
        }

    }

    @Override
    public void valueChanged(ListSelectionEvent e) {


    }
}
