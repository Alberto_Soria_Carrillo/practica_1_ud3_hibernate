package vista;

import bbdd.OjoDrch;
import bbdd.OjoIzq;
import conexion.Conexion;
import constantes.Const;
import main.Modelo;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Clsse VistaOjo, se encarga de la onserción y modificación de los tipos de ojos en la BBDD
 *
 * @author Alberto Soria Carrillo
 */
public class VistaOjo extends JFrame implements ActionListener {

    private JPanel panel1;
    private JCheckBox hipermetropiaCheckBox;
    private JCheckBox astigmatismoCheckBox;
    private JCheckBox reducidoCheckBox;
    private JTextField textField1;
    private JRadioButton normalRadioButton;
    private JRadioButton solRadioButton;
    private JButton anadirOjoButton;
    private JButton cancelarButton;

    private String table;
    private float diotria;
    private Conexion conexion;
    private boolean hipermetropia;
    private boolean astigmatismo;
    private boolean reducido;
    private String tipo;
    private JComboBox combo;
    private boolean aBoolean;
    private Modelo modelo;
    private String info;
    private DefaultListModel defaultListModel;

    /**
     * Contructor de la clase VistaOjo
     *
     * @param table
     * @param conexion
     * @param combo
     */
    public VistaOjo(String table, Conexion conexion, JComboBox combo) {

        this.table = table;
        this.conexion = conexion;
        this.combo = combo;
        aBoolean = false;

        this.add(panel1);

        addActionComand();
        setActionListener(this);

        this.setUndecorated(true);
        this.setDefaultCloseOperation(3);
        this.pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true);

    }

    /**
     * Constructor de la clase Vista Ojo
     *
     * @param info
     * @param table1
     * @param conexion
     * @param modelo
     */
    public VistaOjo(DefaultListModel dlmModificar, String info, String table1, Conexion conexion, Modelo modelo, JComboBox combo) {

        this.aBoolean = true;
        this.table = table1;
        this.conexion = conexion;
        this.modelo = modelo;
        this.info = modelo.sacarId(info);
        this.combo = combo;
        this.anadirOjoButton.setText("Modificar ojo");
        this.defaultListModel = dlmModificar;

        // crea el objeto del elemento a modificar, con la información de la BBDD
        if (Const.TABLE_OJO_IZQ.equalsIgnoreCase(table)) {
            OjoIzq ojoIzq = conexion.buscarOjoIzq(this.info);
            agregarInformacion(ojoIzq);
        }

        if (Const.TABLE_OJO_DRCH.equalsIgnoreCase(table)) {
            OjoDrch ojoDrch = conexion.buscarOjoDrch(this.info);
            agregarInformacion(ojoDrch);
        }

        this.add(panel1);

        addActionComand();
        setActionListener(this);

        this.setUndecorated(true);
        this.setDefaultCloseOperation(3);
        this.pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true);

    }

    /**
     * Agrega la informacion a los campos
     *
     * @param ojo
     */
    private void agregarInformacion(OjoIzq ojo) {
        hipermetropiaCheckBox.setSelected(ojo.isHipermetropia());
        astigmatismoCheckBox.setSelected(ojo.isAstigmatismo());
        reducidoCheckBox.setSelected(ojo.isReducido());
        textField1.setText(String.valueOf(ojo.getGraduacion()));
        normalRadioButton.setSelected(ojo.getTipo().equalsIgnoreCase(Const.TIPO_NORMAL));
        solRadioButton.setSelected(ojo.getTipo().equalsIgnoreCase(Const.TIPO_SOL));
    }

    /**
     * Agrega la informacion a los campos de las tablas
     *
     * @param ojo
     */
    private void agregarInformacion(OjoDrch ojo) {
        hipermetropiaCheckBox.setSelected(ojo.isHipermetropia());
        astigmatismoCheckBox.setSelected(ojo.isAstigmatismo());
        reducidoCheckBox.setSelected(ojo.isReducido());
        textField1.setText(String.valueOf(ojo.getGraduacion()));
        normalRadioButton.setSelected(ojo.getTipo().equalsIgnoreCase(Const.TIPO_NORMAL));
        solRadioButton.setSelected(ojo.getTipo().equalsIgnoreCase(Const.TIPO_SOL));
    }

    /**
     * Agrega los ActionListener
     *
     * @param listener
     */
    private void setActionListener(ActionListener listener) {

        anadirOjoButton.addActionListener(listener);
        cancelarButton.addActionListener(listener);

    }

    /**
     * Agrega los ActionCommand
     */
    private void addActionComand() {

        anadirOjoButton.setActionCommand(Const.BTN_VISTA_OJO);
        cancelarButton.setActionCommand(Const.BTN_CANCELAR);

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        switch (e.getActionCommand()) {
            case Const.BTN_VISTA_OJO:
                if (controlNum()) {
                    hipermetropia = hipermetropiaCheckBox.isSelected();
                    astigmatismo = astigmatismoCheckBox.isSelected();
                    reducido = reducidoCheckBox.isSelected();
                    tipo = seleccionarTipo();

                    // verifica que tabla debe guarda este ojo
                    if (!aBoolean) {
                        if (table.equalsIgnoreCase(Const.TABLE_OJO_DRCH)) {
                            OjoDrch ojoDrch = new OjoDrch();
                            ojoDrch.setHipermetropia(hipermetropia);
                            ojoDrch.setAstigmatismo(astigmatismo);
                            ojoDrch.setReducido(reducido);
                            ojoDrch.setGraduacion(diotria);
                            ojoDrch.setTipo(tipo);
                            conexion.insertInto(ojoDrch);
                        } else {
                            OjoIzq ojoIzq = new OjoIzq();
                            ojoIzq.setHipermetropia(hipermetropia);
                            ojoIzq.setAstigmatismo(astigmatismo);
                            ojoIzq.setReducido(reducido);
                            ojoIzq.setGraduacion(diotria);
                            ojoIzq.setTipo(tipo);
                            conexion.insertInto(ojoIzq);
                        }
                    } else {
                        if (table.equalsIgnoreCase(Const.TABLE_OJO_DRCH)) {
                            OjoDrch ojoDrch = new OjoDrch();
                            ojoDrch.setId(Integer.valueOf(info));
                            ojoDrch.setHipermetropia(hipermetropia);
                            ojoDrch.setAstigmatismo(astigmatismo);
                            ojoDrch.setReducido(reducido);
                            ojoDrch.setGraduacion(diotria);
                            ojoDrch.setTipo(tipo);
                            conexion.update(ojoDrch);
                            conexion.cargarList(this.defaultListModel, table);
                        } else {
                            OjoIzq ojoIzq = new OjoIzq();
                            ojoIzq.setId(Integer.valueOf(info));
                            ojoIzq.setHipermetropia(hipermetropia);
                            ojoIzq.setAstigmatismo(astigmatismo);
                            ojoIzq.setReducido(reducido);
                            ojoIzq.setGraduacion(diotria);
                            ojoIzq.setTipo(tipo);
                            conexion.update(ojoIzq);
                            conexion.cargarList(this.defaultListModel, table);
                        }
                    }

                    conexion.cargarComboBox(combo, table);
                    this.dispose();
                } else {
                    JOptionPane.showMessageDialog(null, "El dato numerico no es valido");
                }
                break;
            case Const.BTN_CANCELAR:
                this.dispose();
                break;
        }

    }

    /**
     * Controla el tipo seleccionado
     *
     * @return
     */
    private String seleccionarTipo() {

        if (solRadioButton.isSelected()) {
            return Const.TIPO_SOL;
        } else {
            return Const.TIPO_NORMAL;
        }

    }

    /**
     * Controla que el dato introducido por en el campo diotria sea numerico
     *
     * @return
     */
    private boolean controlNum() {

        try {
            diotria = Float.valueOf(textField1.getText());
            return true;
        } catch (ArithmeticException | NumberFormatException ex) {
            return false;
        }

    }
}
