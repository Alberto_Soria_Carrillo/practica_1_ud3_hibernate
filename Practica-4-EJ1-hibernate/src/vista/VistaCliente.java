package vista;

import bbdd.Cliente;
import com.github.lgooddatepicker.components.DatePicker;
import conexion.Conexion;
import constantes.Const;
import main.Modelo;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;

/**
 * Clase VistaCliente, permite la creacion ymodificacion de los clientes
 *
 * @author Alberto Soria Carrillo
 */
public class VistaCliente extends JFrame implements ActionListener {
    private JPanel panel1;
    private JTextField nombreTextField;
    private JTextField apellido1TextField;
    private JTextField apellido2TextField;
    private JTextField direccionTextField;
    private JButton anadirClienteButton;
    private JRadioButton hombreRadioButton;
    private JRadioButton mujerRadioButton;
    private DatePicker date;
    private JButton cancelarButton;

    private String table;
    private Conexion conexion;
    private JComboBox comboBox;
    private Boolean aBoolean;
    private Modelo modelo;
    private String info;
    private DefaultListModel defaultListModel;

    /**
     * Constructor de la clase VistaClientes
     *
     * @param tableCliente
     * @param conexion
     * @param clienteFacturaComboBox
     */
    public VistaCliente(String tableCliente, Conexion conexion, JComboBox clienteFacturaComboBox) {

        this.table = tableCliente;
        this.conexion = conexion;
        this.comboBox = clienteFacturaComboBox;
        this.aBoolean = false;

        this.add(panel1);

        addActionComand();
        setActionListener(this);

        this.setUndecorated(true);
        this.setDefaultCloseOperation(3);
        this.pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true);

    }

    /**
     * Contructor de la clase VistClientes
     *
     * @param info
     * @param table
     * @param conexion
     * @param modelo
     */
    public VistaCliente(DefaultListModel defaultListModel, String info, String table, Conexion conexion, Modelo modelo, JComboBox clienteFacturaComboBox) {

        this.modelo = modelo;
        this.info = modelo.sacarId(info);
        this.table = table;
        this.conexion = conexion;
        this.comboBox = clienteFacturaComboBox;
        this.aBoolean = true;
        this.defaultListModel = defaultListModel;
        this.anadirClienteButton.setText("Modificar cliente");

        Cliente cliente = conexion.buscarCliente(this.info);

        agregarInformacion(cliente);

        this.add(panel1);

        addActionComand();
        setActionListener(this);

        this.setUndecorated(true);
        this.setDefaultCloseOperation(3);
        this.pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true);

    }

    /**
     * Agrega informacion a los campos
     *
     * @param cliente
     */
    private void agregarInformacion(Cliente cliente) {

        nombreTextField.setText(cliente.getNombre());
        apellido1TextField.setText(cliente.getApellido1());
        apellido2TextField.setText(cliente.getApellido2());
        direccionTextField.setText(cliente.getDireccion());

        date.setText(formateDate(cliente.getFechaNacimiento().toString()));

        hombreRadioButton.setSelected(cliente.isGenero());
        mujerRadioButton.setSelected(!cliente.isGenero());
    }

    /**
     * Da formato de fecha correcto para agregarlo al DatePicker
     *
     * @param toString
     * @return
     */
    private String formateDate(String toString) {

        String[] aux = toString.split("-");

        String res = "";

        switch (Integer.valueOf(aux[1])) {
            case 1:
                res = aux[2] + " de enero de " + aux[0];
                break;
            case 2:
                res = aux[2] + " de febrero de " + aux[0];
                break;
            case 3:
                res = aux[2] + " de marzo de " + aux[0];
                break;
            case 4:
                res = aux[2] + " de abril de " + aux[0];
                break;
            case 5:
                res = aux[2] + " de mayo de " + aux[0];
                break;
            case 6:
                res = aux[2] + " de junio de " + aux[0];
                break;
            case 7:
                res = aux[2] + " de julio de " + aux[0];
                break;
            case 8:
                res = aux[2] + " de agosto de " + aux[0];
                break;
            case 9:
                res = aux[2] + " de septiembre de " + aux[0];
                break;
            case 10:
                res = aux[2] + " de octubre de " + aux[0];
                break;
            case 11:
                res = aux[2] + " de noviembre de " + aux[0];
                break;
            case 12:
                res = aux[2] + " de diciembre de " + aux[0];
                break;
        }

        return res;

    }

    /**
     * Agrega los ActionListener
     *
     * @param listener
     */
    private void setActionListener(ActionListener listener) {
        anadirClienteButton.addActionListener(listener);
        cancelarButton.addActionListener(listener);
    }

    /**
     * Agrega los ActionCommand
     */
    private void addActionComand() {
        anadirClienteButton.setActionCommand(Const.BTN_ANIDAR_CLIENTE);
        cancelarButton.setActionCommand(Const.BTN_CANCELAR);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case Const.BTN_ANIDAR_CLIENTE:

                if (!nombreTextField.getText().equalsIgnoreCase("")
                        && !apellido1TextField.getText().equalsIgnoreCase("")
                        && !apellido2TextField.getText().equalsIgnoreCase("")
                        && !direccionTextField.getText().equalsIgnoreCase("")
                        && (generoBoolean())
                        && !date.getText().equalsIgnoreCase("")) {

                    if (!aBoolean) {
                        Cliente cliente = new Cliente();

                        cliente.setNombre(nombreTextField.getText());
                        cliente.setApellido1(apellido1TextField.getText());
                        cliente.setApellido2(apellido2TextField.getText());
                        cliente.setDireccion(direccionTextField.getText());
                        cliente.setGenero(generoBoolean());
                        cliente.setFechaNacimiento(Date.valueOf(date.getDate()));

                        conexion.insertInto(cliente);
                    } else {
                        Cliente cliente = new Cliente();

                        cliente.setId(Integer.valueOf(info));
                        cliente.setNombre(nombreTextField.getText());
                        cliente.setApellido1(apellido1TextField.getText());
                        cliente.setApellido2(apellido2TextField.getText());
                        cliente.setDireccion(direccionTextField.getText());
                        cliente.setGenero(generoBoolean());
                        cliente.setFechaNacimiento(Date.valueOf(date.getDate()));

                        conexion.update(cliente);
                        conexion.cargarList(this.defaultListModel, table);
                    }
                    conexion.cargarComboBox(comboBox, table);
                    this.dispose();

                } else {
                    JOptionPane.showMessageDialog(null, "Se deben rellenar todos los campos");
                }

                break;
            case Const.BTN_CANCELAR:
                this.dispose();
                break;
        }
    }

    /**
     * Verifica el genero
     *
     * @return
     */
    private String generoString() {

        if (hombreRadioButton.isSelected()) {
            return "true";
        } else {
            return "false";
        }
    }

    /**
     * Verifica el genero
     *
     * @return
     */
    private boolean generoBoolean() {

        if (hombreRadioButton.isSelected()) {
            return true;
        }
        if (mujerRadioButton.isSelected()) {
            return true;
        }

        return false;
    }


}
