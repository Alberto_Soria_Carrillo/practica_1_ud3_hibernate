package main;

import conexion.Conexion;
import vista.VistaFactura;

/**
 * Clase Principal, se encarga del arranque del programa
 *
 * @author Alberto Soria Carrillo
 */
public class Principal {
    /**
     * Metodo main del programa
     *
     * @param args
     */
    public static void main(String[] args) {

        Modelo modelo = new Modelo();
        Conexion conexion = new Conexion();
        VistaFactura vista = new VistaFactura();
        Controlador controlador = new Controlador(modelo, vista, conexion);

    }

}
