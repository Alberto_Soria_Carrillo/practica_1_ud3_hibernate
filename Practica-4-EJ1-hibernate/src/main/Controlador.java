package main;

import conexion.Conexion;
import constantes.Const;
import vista.*;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Clase Controlador, controla la intercción entre las clases del programa
 *
 * @author Alberto Soria Carrillo
 */
public class Controlador implements ActionListener, ListSelectionListener {

    private Modelo modelo;
    private VistaFactura vista;
    private Conexion conexion;
    private VistaOjo vistaOjo;
    private VistaMontura vistaMontura;
    private VistaCliente vistaCliente;
    private VistaModificar vistaModificar;

    /**
     * Constructor de la Clase Controlador
     *
     * @param modelo
     * @param vista
     * @param conexion
     */
    public Controlador(Modelo modelo, VistaFactura vista, Conexion conexion) {
        this.modelo = modelo;
        this.vista = vista;
        this.conexion = conexion;

        setActionListener(this);
        addListSelectionListener(this);
        addActionComand();

    }

    /**
     * Agrega los ActionCommand
     */
    private void addActionComand() {
        vista.anadirCristalesIzquierdosButton.setActionCommand(Const.BTN_ANIDAR_CRISTALES_IZQ);
        vista.anadirCristalesDerechosButton.setActionCommand(Const.BTN_ANIDAR_CRISTALES_DRCH);
        vista.seleccionarClienteButton.setActionCommand(Const.BTN_SELECCIONAR_CLIENTE);
        vista.anadirMonturasButton.setActionCommand(Const.BTN_ANIDAR_MONTURA);
        vista.crearFacturaButton.setActionCommand(Const.BTN_CREAR_FACTURA);
        vista.eliminarSeleccionButton.setActionCommand(Const.BTN_ELIMINAR_SELECCION);
        vista.crearClienteButton.setActionCommand(Const.BTN_CREAR_CLIENTE);
        vista.crearCristalIzquierdoButton.setActionCommand(Const.BTN_CREAR_CRISTAL_IZQUIERDO);
        vista.crearCristalDerechoButton.setActionCommand(Const.BTN_CREAR_CRISTAL_DERECHO);
        vista.crearMonturaButton.setActionCommand(Const.BTN_CREAR_MONTURA);
        vista.modificarClienteButton.setActionCommand(Const.BTN_MODIFICAR_CLIENTE);
        vista.modificarCristalIzquierdoButton.setActionCommand(Const.BTN_MODIFICAR_CRISTAL_IZQUIERDO);
        vista.modificarCristalDerechoButton.setActionCommand(Const.BTN_MODIFICAR_CRISTAL_DERECHO);
        vista.modificarMonturaButton.setActionCommand(Const.BTN_MODIFICAR_MONTURA);
        vista.item.setActionCommand(Const.BTN_CONECTAR);

    }

    /**
     * Agrega los ListSelectionListener
     *
     * @param listener
     */
    private void addListSelectionListener(ListSelectionListener listener) {

        vista.ojoIzqFacturaList.addListSelectionListener(listener);
        vista.ojoDrchFacturaList.addListSelectionListener(listener);
        vista.monturaFacturaList.addListSelectionListener(listener);
        vista.facturaList.addListSelectionListener(listener);

    }

    /**
     * Agrega los ActionListener
     *
     * @param listener
     */
    private void setActionListener(ActionListener listener) {
        vista.anadirCristalesIzquierdosButton.addActionListener(listener);
        vista.anadirCristalesDerechosButton.addActionListener(listener);
        vista.seleccionarClienteButton.addActionListener(listener);
        vista.anadirMonturasButton.addActionListener(listener);
        vista.crearFacturaButton.addActionListener(listener);
        vista.eliminarSeleccionButton.addActionListener(listener);
        vista.crearClienteButton.addActionListener(listener);
        vista.crearCristalIzquierdoButton.addActionListener(listener);
        vista.crearCristalDerechoButton.addActionListener(listener);
        vista.crearMonturaButton.addActionListener(listener);
        vista.modificarClienteButton.addActionListener(listener);
        vista.modificarCristalIzquierdoButton.addActionListener(listener);
        vista.modificarCristalDerechoButton.addActionListener(listener);
        vista.modificarMonturaButton.addActionListener(listener);
        vista.item.addActionListener(listener);

        vista.ojoDrchFacturaComboBox.addActionListener(listener);
        vista.monturaComboBox.addActionListener(listener);
        vista.clienteFacturaComboBox.addActionListener(listener);
        vista.ojoIzqFacturaComboBox.addActionListener(listener);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        switch (e.getActionCommand()) {
            case Const.BTN_SELECCIONAR_CLIENTE:
                vista.nombreClienteFacturaTextField.setText((vista.clienteFacturaComboBox.getItemAt((vista.clienteFacturaComboBox.getSelectedIndex()))).toString());
                break;
            case Const.BTN_ANIDAR_CRISTALES_IZQ:
                vista.dlmOjo_izq.addElement((vista.ojoIzqFacturaComboBox.getItemAt(vista.ojoIzqFacturaComboBox.getSelectedIndex())));
                break;
            case Const.BTN_ANIDAR_CRISTALES_DRCH:
                vista.dlmOjo_drch.addElement((vista.ojoDrchFacturaComboBox.getItemAt(vista.ojoDrchFacturaComboBox.getSelectedIndex())));
                break;
            case Const.BTN_ANIDAR_MONTURA:
                vista.dlmMontura.addElement((vista.monturaComboBox.getItemAt(vista.monturaComboBox.getSelectedIndex())));
                break;
            case Const.BTN_CREAR_FACTURA:
                modelo.createFact(vista,conexion);
                modelo.agreagarInformacion(vista, conexion);
                break;
            case Const.BTN_CREAR_CLIENTE:
                vistaCliente = new VistaCliente(Const.TABLE_CLIENTE, conexion, vista.clienteFacturaComboBox);
                modelo.agreagarInformacion(vista, conexion);
                break;
            case Const.BTN_CREAR_CRISTAL_IZQUIERDO:
                vistaOjo = new VistaOjo(Const.TABLE_OJO_IZQ, conexion, vista.ojoIzqFacturaComboBox);
                modelo.agreagarInformacion(vista, conexion);
                break;
            case Const.BTN_CREAR_CRISTAL_DERECHO:
                vistaOjo = new VistaOjo(Const.TABLE_OJO_DRCH, conexion, vista.ojoDrchFacturaComboBox);
                modelo.agreagarInformacion(vista, conexion);
                break;
            case Const.BTN_CREAR_MONTURA:
                vistaMontura = new VistaMontura(Const.TABLE_MONTURA, conexion, vista.monturaComboBox);
                modelo.agreagarInformacion(vista, conexion);
                break;
            case Const.BTN_MODIFICAR_CLIENTE:
                vistaModificar = new VistaModificar(conexion,Const.TABLE_CLIENTE,modelo,vista);
                break;
            case Const.BTN_MODIFICAR_CRISTAL_IZQUIERDO:
                vistaModificar = new VistaModificar(conexion,Const.TABLE_OJO_IZQ,modelo,vista);
                break;
            case Const.BTN_MODIFICAR_CRISTAL_DERECHO:
                vistaModificar = new VistaModificar(conexion,Const.TABLE_OJO_DRCH,modelo,vista);
                break;
            case Const.BTN_MODIFICAR_MONTURA:
                vistaModificar = new VistaModificar(conexion,Const.TABLE_MONTURA,modelo, vista);
                break;
            case Const.BTN_ELIMINAR_SELECCION:
                modelo.limpiarInformacion(vista);
                modelo.agreagarInformacion(vista,conexion);
                break;
            case Const.BTN_CONECTAR:
                if (conexion.sessionFactory == null || conexion.sessionFactory.isClosed()) {
                    conexion.conectar();
                    vista.item.setText(Const.BTN_DESCONECTAR);
                    modelo.agreagarInformacion(vista, conexion);
                } else if (conexion.sessionFactory.isOpen()) {
                    conexion.desconectar();
                    vista.item.setText(Const.BTN_CONECTAR);
                    modelo.limpiarInformacion(vista);
                }
                break;
        }

    }

    @Override
    public void valueChanged(ListSelectionEvent e) {

    }

}
