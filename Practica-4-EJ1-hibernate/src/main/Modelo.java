package main;

import bbdd.*;
import conexion.Conexion;
import constantes.Const;
import vista.VistaFactura;

import javax.swing.*;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Clase Modelo, se encarga de la ejecucion de las accion es del programa
 *
 * @author Alberto Soria Carrillo
 */
public class Modelo {

    /**
     * Carga la información en los Jlist y los JComboBox
     *
     * @param vista
     * @param conexion
     */
    public void agreagarInformacion(VistaFactura vista, Conexion conexion) {

        conexion.cargarComboBox(vista.ojoDrchFacturaComboBox, Const.TABLE_OJO_DRCH);
        conexion.cargarComboBox(vista.ojoIzqFacturaComboBox, Const.TABLE_OJO_IZQ);
        conexion.cargarComboBox(vista.clienteFacturaComboBox, Const.TABLE_CLIENTE);
        conexion.cargarComboBox(vista.monturaComboBox, Const.TABLE_MONTURA);

        conexion.cargarList(vista.dlmFactura, Const.TABLE_FACTURA);
    }

    /**
     * Limpia la información de los Jlist y de los JComboBox
     *
     * @param vista
     */
    public void limpiarInformacion(VistaFactura vista) {
        limpiarTabla(vista.dlmMontura);
        limpiarTabla(vista.dlmOjo_drch);
        limpiarTabla(vista.dlmOjo_izq);
        limpiarTabla(vista.dlmFactura);

        vista.nombreClienteFacturaTextField.setText("");

        limpiarComboBox(vista.ojoDrchFacturaComboBox);
        limpiarComboBox(vista.ojoIzqFacturaComboBox);
        limpiarComboBox(vista.clienteFacturaComboBox);
        limpiarComboBox(vista.monturaComboBox);
    }

    /**
     * Limpia el JComboBox
     *
     * @param comboBox
     */
    private void limpiarComboBox(JComboBox comboBox) {
        comboBox.removeAllItems();
    }

    /**
     * Limpia el Jlist
     *
     * @param list
     */
    private void limpiarTabla(DefaultListModel list) {
        list.removeAllElements();
    }

    /**
     * Fracciona el String y debuelve el primer fragmento
     *
     * @param string
     * @return
     */
    public String sacarId(String string) {
        String[] aux = string.split(" - ");
        String id = aux[0];
        return id;

    }

    /**
     * Crea la factura con los datos de los Jlist e invoca el metodo para guardarla en la BBDD
     *
     * @param vista
     * @param conexion
     */
    public void createFact(VistaFactura vista, Conexion conexion) {

        Factura factura = new Factura();

        Cliente cliente = conexion.buscarCliente(sacarId(vista.nombreClienteFacturaTextField.getText()));
        ArrayList<Montura> monturas = conexion.buscarMontura(vista.dlmMontura);
        ArrayList<OjoDrch> ojoDrches = conexion.buscarOjoDrch(vista.dlmOjo_drch);
        ArrayList<OjoIzq> ojoizq = conexion.buscarOjoIzq(vista.dlmOjo_izq);


        factura.setNameOptica("Opticas Perez");
        factura.setDireccion("Calle Los Olmos");
        factura.setTelefono("976-68-67-66");
        factura.setCliente(cliente);
        factura.setFecha(Date.valueOf(LocalDate.now()));
        factura.setPrecioTotal(price(monturas));

        conexion.insertInto(factura);

        factura.setId(conexion.maxIdFactura());

        factura.setMontura(monturas);
        factura.setOjo_drch(ojoDrches);
        factura.setOjo_izq(ojoizq);

        conexion.update(factura);

    }

    /**
     * Calcula el precio
     *
     * @param monturas
     * @return
     */
    private double price(ArrayList<Montura> monturas) {

        double price = 0;

        for (Montura aux : monturas) {
            price = price + aux.getPrecio();
        }

        return price;
    }

    /**
     * Comprueba si el dato introducidos es un valor numérico
     *
     * @param text
     * @return
     */
    public boolean comprobarInt(String text) {

        try {
            Integer.valueOf(text);
            return true;
        } catch (ArithmeticException | NumberFormatException e) {
            return false;
        }

    }
}
